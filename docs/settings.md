# Settings

You can for a nu,ber of steps change the behavior of aardvark-py for
your repository. This is done by adding configuration files to
your repository.

## Control what to test and other Pytest behaviors

By default all tests in directory tests runs. IF you will limit
it `pytest.ini` to overide it. Read more in the
[Pytest documentation](https://docs.pytest.org/en/stable/reference/customize.html).


## Coverage

Read how to tune the configuration for coverage in
[pytest-coverage documentation](https://pytest-cov.readthedocs.io/en/latest/config.html)

To omit tests in coverage report put in `.coveragerc`:

```ini
[run]
omit = tests/*
```
