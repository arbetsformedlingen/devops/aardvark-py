# Aardvark-py

This project is an extension to the [Aardvark project](https://gitlab.com/arbetsformedlingen/devops/aardvark).
It builds and publish Python packages.

The pipeline is highly opinionated. One can either use only the
build pipeline or both build and deployment pipeline.

## TODO

* Secure and add resource limits to tasks
* Feedback to Mattermost
* Check style
